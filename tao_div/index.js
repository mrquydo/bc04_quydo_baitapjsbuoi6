function createDiv() {
  var result = document.getElementById("result");

  var divList = "";

  for (i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      divList += `<div style= "color: white; background: red; height:20px">Div chẵn ${i}</div>`;
    } else {
      divList += `<div style= "color: white; background: blue; height:20px">Div lẻ ${i}</div>`;
    }
  }

  result.innerHTML = divList;
}
